#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Read;
use std::io::{Error, ErrorKind};

#[macro_use]
extern crate lazy_static;
use regex::Regex;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let passports = build_passports(contents.as_str())?;

    println!("{}", day2(passports));

    Ok(())
}

fn day1(passports: Vec<Passport>) -> usize {
    passports.iter().map(|p| p.is_valid1()).fold(0, |acc, b| match b {
        true => acc + 1,
        false => acc
    })
}

fn day2(passports: Vec<Passport>) -> usize {
    passports.iter().map(|p| p.is_valid2()).fold(0, |acc, b| match b {
        true => acc + 1,
        false => acc
    })
}

struct Passport {
    byr: Option<usize>,
    iyr: Option<usize>,
    eyr: Option<usize>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
}

impl Passport {
    fn new() -> Passport {
        Passport {byr: None, iyr: None, eyr: None, hgt: None, hcl: None, ecl: None, pid: None, cid: None}
    }

    fn is_valid1(&self) -> bool {
        self.byr.is_some() && self.iyr.is_some() && self.eyr.is_some() && self.hgt.is_some() && self.hcl.is_some() && self.ecl.is_some() && self.pid.is_some()
    }

    fn is_valid2(&self) -> bool {
        self.years_valid() && self.height_valid() && self.hair_valid() && self.eye_valid() && self.pid_valid()
    }

    fn years_valid(&self) -> bool {
        match self.byr {
            Some(n) => if !(1920 <= n && n <= 2002) {return false},
            None => return false
        };
        match self.iyr {
            Some(n) => if !(2010 <= n && n <= 2020) {return false},
            None => return false
        };
        match self.eyr {
            Some(n) => if !(2020 <= n && n <= 2030) {return false},
            None => return false
        };
        
        true
    }

    fn height_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^\d+(in|cm)$").unwrap();
        }
        if self.hgt.is_none() {
            return false;
        }
        if !RE.is_match(self.hgt.as_ref().unwrap().as_str()) {
            return false;
        }
        let parts: Vec<&str> = self.hgt.as_ref().unwrap().split(|c| c == 'i' || c == 'c').collect();
        let (num, unit) = (parts[0].parse::<usize>().unwrap(), parts[1]);
        match unit { // the split gobbles the first char of in and cm so judge by the second char
            "m" => if 150 <= num && num <= 193 {return true} else {return false},
            "n" => if 59 <= num && num <= 76 {return true} else {return false},
            _ => false
        }
    }

    fn hair_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^#[0-9a-fA-F]{6}$").unwrap();
        }
        self.hcl.is_some() && RE.is_match(self.hcl.as_ref().unwrap())
    }

    fn eye_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^amb|blu|brn|gry|grn|hzl|oth$").unwrap();
        }
        self.ecl.is_some() && RE.is_match(self.ecl.as_ref().unwrap())
    }

    fn pid_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^\d{9}$").unwrap();
        }
        self.pid.is_some() && RE.is_match(self.pid.as_ref().unwrap())
    }
}

fn build_passports(raw: &str) -> Result<Vec<Passport>, Error> {
    let raw_passports: Vec<&str> = raw.split("\n\n").collect();
    let mut passports = Vec::new();
    for passport in raw_passports {
        let mut obj = Passport::new();
        for segment in passport.split(|c| c == '\n' || c == ' ') {
            let parts: Vec<&str> = segment.split(":").collect();
            if parts == vec![""] {continue}
            let (id, content) = (parts[0], parts[1]);
            match id {
                "byr" => obj.byr = Some(content.parse::<usize>().unwrap()),
                "iyr" => obj.iyr = Some(content.parse::<usize>().unwrap()),
                "eyr" => obj.eyr = Some(content.parse::<usize>().unwrap()),
                "hgt" => obj.hgt = Some(content.to_string()),
                "hcl" => obj.hcl = Some(content.to_string()),
                "ecl" => obj.ecl = Some(content.to_string()),
                "pid" => obj.pid = Some(content.to_string()),
                "cid" => obj.cid = Some(content.to_string()),
                _ => return Err(Error::new(ErrorKind::Other, "unrecognised field"))
            }
        }
        passports.push(obj);
    }
    Ok(passports)
}