#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]
use std::fs::File;
use std::collections::HashSet;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Read;

fn main() -> Result<(), std::io::Error> {
    let mut file = File::open("input_t.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;


    let groups = Group::build(&contents);
    
    day2(groups);
    
    Ok(())
}

fn day1(groups: Vec<Group>) {
    let total: usize = groups.iter().map(|p| p.all_union().len()).sum();
    println!("{}", total);
}

fn day2(groups: Vec<Group>) {
    let total: usize = groups.iter().map(|p| p.all_intersection().len()).sum();
    println!("{}", total);
}

struct Group {
    people: Vec<Person>
}

impl Group {
    fn build(input: &str) -> Vec<Group> {
        let mut groups = Vec::new();
        for group in input.split("\n\n") {
            let mut people = Vec::new();
            for person in group.lines() {
                people.push(Person::build(person));
            }
            groups.push(Group { people });
        };
        groups
    }

    fn all_union(&self) -> HashSet<char> {
        let mut all = HashSet::new();
        for person in &self.people {
            all = all.union(&person.answers).map(|x| *x).collect();
        }

        all
    }

    fn all_intersection(&self) -> HashSet<char> {
        let mut iter = self.people.iter().map(|p| p.answers);
        let intersection = iter.next().map(|set| iter.fold(set, |set1, set2| set1 & set2));
        intersection.unwrap()
    }
}

#[derive(Clone)]
struct Person {
    answers: HashSet<char>
}

impl Person {
    fn build(input: &str) -> Person {
        let mut answers = HashSet::new();
        for c in input.chars() {
            answers.insert(c);
        }
        Person { answers }
    }
}