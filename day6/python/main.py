def main():
    with open("input.txt") as file:
        contents = file.read()
    groups = Group.build(contents)
    acc = 0
    for group in groups: 
        acc += len(group.all_intersection())
    print(acc)
    

class Group():
    def __init__(self, people):
        self.people = people
    
    def all_intersection(self):
        return self.people[0].intersection(*self.people)
        
    def build(build_in):
        groups = []
        for group in build_in.split("\n\n"):
            people = []
            for person in group.splitlines():
                people.append(make_person(person))
            groups.append(Group(people))
        return groups
    
    def __str__(self):
        return str(self.people)
    
def make_person(person):
    answers = set()
    for c in person:
        answers.add(c)
    return answers


if __name__ == "__main__":
    main()