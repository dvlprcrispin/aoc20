#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
use std::collections::HashSet;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

fn main() -> std::io::Result<()> {
    part2()
}

fn part1() -> std::io::Result<()> {
    let file = File::open("input1.txt")?;

    let mut stuff: HashSet<usize> = HashSet::new();
    
    let buf = BufReader::new(file);

    for line in buf.lines() {
        stuff.insert(line?.parse::<usize>().unwrap());
    }

    for x in stuff.iter() {
        for y in stuff.iter() {
            if x + y == 2020 {
                println!("{}", x * y);
            }
        }

    }

    Ok(())
}

fn part2() -> std::io::Result<()> {
    let file = File::open("input1.txt")?;

    let mut stuff: HashSet<usize> = HashSet::new();
    
    let buf = BufReader::new(file);

    for line in buf.lines() {
        stuff.insert(line?.parse::<usize>().unwrap());
    }

    for x in stuff.iter() {
        for y in stuff.iter() {
            for z in stuff.iter() {
                if x + y + z == 2020 {
                    println!("{}", x * y * z);
                }
            }
        }

    }

    Ok(())
}
