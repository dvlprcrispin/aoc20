#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::io::{Error, ErrorKind};

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let buf = BufReader::new(file);

    let terrain = build_terrain(buf)?;

    let count = part2(terrain)?;
    println!("{}", count);

    Ok(())
}

fn part1(terrain: Terrain) -> Result<usize, Error> {
    let mut count = 0;
    for y in 1 .. terrain.height {
        match terrain.at(3*y, y)? {
            Ground::Tree => count += 1,
            _ => {}
        }
    }
    
    Ok(count)
}

fn part2(terrain: Terrain) -> Result<usize, Error> {
    let checks = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let map = checks.iter().map(|n| terrain.traverse(n.0, n.1).unwrap());
    for n in map.clone() {
        print!("{} ", n);
    }
    Ok(map.product::<usize>())
}


#[derive(Debug)]
enum Ground {
    Open,
    Tree
}

struct Terrain {
    map: Vec<String>,
    width: usize,
    height: usize
}

impl Terrain {
    fn at(&self, x: usize, y: usize) -> Result<Ground, Error> {
        let line = self.map.get(y).unwrap();
        match get_at(line, x % self.width) {
            Ok(c) => match c {
                '.' => return Ok(Ground::Open),
                '#' => return Ok(Ground::Tree),
                _ => return Err(Error::new(ErrorKind::Other, "the map's really weird"))
            },
            Err(e) => return Err(Error::new(ErrorKind::Other, e))
        }
        
    }

    fn print(&self) -> Result<(), Error> {
        for y in 0 .. self.height {
            let mut s = String::new();
            for x in 0 .. 66 {
                match self.at(x, y)? {
                    Ground::Open => s.push('.'),
                    Ground::Tree => s.push('#')
                }
            }
            println!("{}", s);
        }
        Ok(())
    }

    fn traverse(&self, x_dist: usize, y_dist: usize) -> Result<usize, Error> {
        let mut count = 0;
        let (mut x, mut y) = (0, 0);
        while y < self.height {
            match self.at(x, y)? {
                Ground::Tree => count += 1,
                _ => {}
            }
            x += x_dist;
            y += y_dist;
        }
        Ok(count)
    }
}


fn build_terrain(buf: BufReader<File>) -> Result<Terrain, Error> {
    let mut map = Vec::new();
    for line in buf.lines() {
        map.push(line?);
    }
    let width = map[0].len();
    let height = map.len();
    Ok(Terrain { map, width, height })
}

fn get_at(s: &String, i: usize) -> Result<char, String> {
    match s.as_str().chars().nth(i) {
        Some(c) => return Ok(c),
        None => return Err(format!("Index {} oob probably", i))
    }
}