#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]
use std::fs::File;
use std::collections::HashMap;
use std::io::BufReader;
use std::io::BufRead;
use std::io::{Error, ErrorKind};
use std::hash::Hash;


fn main() -> Result<(), std::io::Error> {
    let file = File::open("input.txt")?;
    let buf = BufReader::new(file);

    let seats = Seat::build(buf)?;
    day2(seats);
    Ok(())
}

fn day1(seats: Vec<Seat>) {
    let highest = seats.iter().map(|s| s.id).max().unwrap();
    println!("{}", highest);
}

fn day2(seats: Vec<Seat>) {
    let mut map = HashMap::new();
    for seat in seats {
        inc_or_ins(&mut map, (seat.row - 1, seat.column));
        inc_or_ins(&mut map, (seat.row + 1, seat.column));
    }
    let valids: Vec<Seat> = map.iter().filter(|n| n.1 < &2).map(|n| Seat::from_nums(n.0.0, n.0.1)).collect();
    let mut ids: Vec<isize> = valids.iter().map(|s| s.id).collect();
    ids.sort(); // at this point look at them to find the gap in ids
    println!("{:?}", ids);
    let interesing_seats: Vec<&Seat> = valids.iter().filter(|s| s.id == 544 || s.id == 560).collect();
    println!("{:?}", interesing_seats); // at this point look at them to find which one we're missing
    println!("{:?}", Seat::from_nums(69, 0));
}

fn inc_or_ins<T: Eq + Hash>(map: &mut HashMap<T, isize>, key: T) {
    let n = map.entry(key).or_insert(0);
    *n += 1;
}

#[derive(Debug)]
struct Seat {
    row: isize,
    column: isize,
    id: isize
}

impl Seat {
    fn new(raw: &str) -> Result<Seat, std::num::ParseIntError> {
        let (c_row, c_col) = raw.split_at(7);
        let s_row = c_row.replace('F', "0").replace('B', "1");
        let s_col = c_col.replace('L', "0").replace('R', "1");

        let row = isize::from_str_radix(&s_row, 2)?;
        let column = isize::from_str_radix(&s_col, 2)?;

        Ok(Seat {row, column, id: row * 8 + column})
    }

    fn from_nums(row: isize, column: isize) -> Seat {
        Seat {row, column, id: row * 8 + column}
    }

    fn build(buf: BufReader<File>) -> Result<Vec<Seat>, Error> {
        let mut results = Vec::new();
        for line in buf.lines() {
            match Seat::new(&line?) {
                Ok(s) => results.push(s),
                Err(e) => return Err(Error::new(ErrorKind::Other, e))
            }
            
        }
        Ok(results)
    }
}