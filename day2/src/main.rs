#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;




fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let buf = BufReader::new(file);
    

    let mut passes = Vec::new();
    for line in buf.lines() {
        passes.push(build_pass(line?.as_str()));
    }

    let valids = passes.iter().map(|p| is_valid_2(p)).fold(0, |acc, b| match b {
        true => acc + 1,
        false => acc
    });
    println!("{:#?}", valids);

    Ok(())
}

#[derive(Debug)]
struct Pass {
    low: usize,
    high: usize,
    accepted: char,
    map_pass: HashMap<char, usize>,
    pass: String
}

fn build_pass(raw: &str) -> Pass {
    let split1: Vec<&str> = raw.split(": ").collect();
    let left = split1[0]; let right = split1[1];

    let leftsplit: Vec<&str> = left.split(" ").collect();
    let range = leftsplit[0]; let accepted = leftsplit[1].chars().next().unwrap();
    
    let rangesplit: Vec<usize> = range.split("-").map(|x| x.parse::<usize>().unwrap()).collect();
    let low = rangesplit[0]; let high = rangesplit[1];
    
    let mut map_pass = HashMap::new();

    for c in right.chars() {
        let n = map_pass.entry(c).or_insert(0);
        *n += 1;
    }

    Pass { low, high, accepted, map_pass, pass: right.to_string() }
}

fn is_valid_1(pass: &Pass) -> bool {
    let has = match pass.map_pass.get(&pass.accepted) {
        Some(n) => n,
        None => return false
    };
    pass.low <= *has && *has <= pass.high
}

fn is_valid_2(pass: &Pass) -> bool {
    let mut raw = pass.pass.as_str().chars();

    let mut num_matching = 0;

    if raw.clone().nth(pass.low - 1).unwrap_or('0') == pass.accepted { num_matching += 1}
    if raw.clone().nth(pass.high - 1).unwrap_or('0') == pass.accepted { num_matching += 1}
    // println!("{:?}, {}", raw.nth(pass.low + 1), pass.accepted);
    

    num_matching == 1
}